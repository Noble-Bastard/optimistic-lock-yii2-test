<?php
/* @var $this yii\web\View */

use yii\widgets\ActiveForm;
use yii\helpers\Html;

$this->title = 'Добавить задачу';

?>

<div id='modalContent'>
    <div class="page-feedback">
        <h1><?= Html::encode($this->title) ?></h1>

        <?php $form = ActiveForm::begin(); ?>
        <?= $form->field($model, 'header')->input('string', ['required' => '']); ?>

        <?= $form->field($model, 'priority')->dropDownList(
            [
                '1' => 'низкий',
                '2' => 'средний',
                '3' => 'высокий'
            ]
        )
        ?>
        <?= $form->field($model, 'status')->checkbox(
            [
                'checked' => false
            ]
        ); ?>

        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']); ?>
        <?php ActiveForm::end(); ?>
    </div>
</div>