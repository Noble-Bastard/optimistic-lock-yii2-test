<?php
/* @var $this yii\web\View */

use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;

?>

<div>
    <h1><?= Html::encode('Редактировать задачу') ?></h1>


    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'header')->input('string', ['required' => '']); ?>

    <?= $form->field($model, 'priority')->dropDownList(
        [
            '1' => 'низкий',
            '2' => 'средний',
            '3' => 'высокий'
        ]
    )
    ?>
    <?= $form->field($model, 'status')->checkbox()->label('Готово'); ?>
    <?= $form->field($model, 'version')->hiddenInput()->label(false); ?>
    <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']); ?>
    <?php ActiveForm::end(); ?>
</div>