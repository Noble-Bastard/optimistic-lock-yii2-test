<?php

/** @var yii\web\View $this */

use yii\helpers\Url;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\bootstrap4\Modal;

?>

<h1>TO DO APP</h1>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'summary' => false,
    'pager' => [
        'options' => [
            'tag' => 'ul',
            'class' => 'pagination pagination-sm',
        ]
    ],
    'columns' => [
        [
            'attribute' => 'header',
            'value' => function ($data) {
                return "<span class='grid-cell-gray'>" . $data->header . "</span>";
            },
            'format' => 'html',
            'filter' => false
        ],
        [
            'attribute' => 'priority',
            'value' => function ($data) {
                switch ($data->priority) {
                    case 1:
                        $priority = 'низкий';
                        break;
                    case 2:
                        $priority = 'средний';
                        break;
                    case 3:
                        $priority = 'высокий';
                        break;
                }
                return $priority;
            },
            'filter' => false
        ],
        [
            'attribute' => 'status',
            'value' => function ($data) {
                switch ($data->status) {
                    case 0:
                        $status = 'к выполнению';
                        break;
                    case 1:
                        $status = 'выполнена';
                        break;
                    default:
                        $status = 'к выполнению';
                        break;
                }
                return $status;
            },
            'filter' => false
        ],
        [
            'label' => 'Действия',
            'format' => 'raw',
            'value' => function ($data) {
                return Html::button(
                    "Редактировать",
                    [
                        'value' => Url::to("/site/update?id=$data->id"),
                        'id' => 'modalButtonUpdate' . $data->id,
                        'class' => 'btn btn-primary not-button'
                    ]
                );
            }
        ],
    ],
]); ?>

<?php
Modal::begin([
    'id' => 'modalUpdate',
    'size' => 'modal-lg'
]);
echo "<div id='modalContent1'></div>";
Modal::end();
?>

<?=
Html::button("Новая задача", ['value' => Url::to('/site/insert'), 'class' => 'btn btn-primary', 'id' => 'modalButton'])
?>
<?php
Modal::begin([
    'id' => 'modal',
    'size' => 'modal-lg'
]);
echo "<div id='modalContent'></div>";
Modal::end();
?>