<?php

namespace app\models;

use yii\behaviors\OptimisticLockBehavior;

/**
 * This is the model class for table "tasks".
 *
 * @property int $id
 * @property string $header
 * @property int $created_at
 * @property int $updated_at
 * @property int $priority
 * @property int $status
 * @property int $version
 * 
 */
class Tasks extends \yii\db\ActiveRecord
{
    public function behaviors()
    {
        return [
            OptimisticLockBehavior::class,
        ];
    }
    
    public function optimisticLock()
    {
        return 'version';
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tasks';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['header'], 'safe'],
            [['priority', 'status'], 'required'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'header' => 'Заголовок',
            'priority' => 'Приоретет',
            'status' => 'Статус',
        ];
    }
}
