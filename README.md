TODO List

### База данных
Подключение к базе данных натсраивается в файле config/db.php

### Миграции
После настройки подключения к базе данных выполнить миграции командой ./yii migrate.

### Запуск
./yii serve

### Работа с REST API к полю Field
#### Make Done
```
http://localhost:8080/make/done?name=Task1
```


#### Make Not Done
```
http://localhost:8080/make/not-done?name=Task1
```