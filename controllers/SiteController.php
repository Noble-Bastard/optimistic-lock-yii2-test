<?php

namespace app\controllers;

use app\models\Tasks;
use app\models\TasksSearch;
use Yii;
use yii\db\StaleObjectException;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new TasksSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->get());
        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionInsert()
    {
        $model = new Tasks();
        if ($model->load(Yii::$app->request->post())) {
            $model->created_at = time();
            $model->updated_at = time();
            $model->save();
            return $this->redirect(['/']);
        } else {
            return $this->renderAjax('insert', ['model' => $model]);
        }
    }

    public function actionUpdate($id)
    {
        $model = Tasks::findOne($id);
        try {
            if ($model->load(Yii::$app->request->post())) {
                $model->updated_at = time();
                if ($model->save()) {
                    Yii::$app->session->setFlash('success', 'Record updated successfully.');
                } else {
                    Yii::$app->session->setFlash('error', 'Failed to update record.');
                }
                return $this->redirect(['/']);
            }
        } catch (StaleObjectException $e) {
            Yii::$app->session->setFlash('error', 'Record has been modified by another user. Please reload the record and try again.');
            return $this->redirect(['/']);
        }
        return $this->renderAjax('update', ['model' => $model]);
    }
}
