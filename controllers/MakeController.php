<?php

namespace app\controllers;

use app\models\Tasks;
use yii\db\StaleObjectException;
use Yii;
use yii\rest\Controller;
use yii\helpers\Json;
use yii\web\NotFoundHttpException;

/**
 * Summary of MakeController
 */
class MakeController extends Controller
{
    public function actionDone($name)
    {
        $model = $this->findTask($name);
        return $this->update($model, 1);
    }

    public function actionNotDone($name)
    {
        $model = $this->findTask($name);
        return $this->update($model, 0);
    }

    private function update($model, $status)
    {
        try {
            if ($model->updateAttributes(['status' => $status])) {
                return Json::encode([
                    'message' => 'success'
                ]);
            } else {
                return Json::encode([
                    'message' => 'Already updated like that'
                ]);
            }
        } catch (StaleObjectException $e) {
            return Json::encode([
                'message' => 'Error'
            ]);
        }
    }

    protected function findTask($name)
    {
        if (($model = Tasks::findOne(['header' => $name])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
